# Installation
## Installation Modes
### Local Installation
Local installation with `pip` requires one Python package only for all services:

```shell
pip install otter-grader-service --index-url https://gitlab.ethz.ch/api/v4/projects/45207/packages/pypi/simple
```

Launch commands:
- **Pusher:** `gunicorn -b 0.0.0.0:8000 -t 120 -w 4 --worker-class uvicorn.workers.UvicornWorker otter_grader_service.pusher.main:app`
- **Grader:** `python -m otter_grader_service.grader.main`
- **Responder:** `python -m otter_grader_service.responder.main`

See also the systemd unit for the grader component.

Multiple graders can be launched on a single host.

### Docker
As a starting point, see the
[Docker compose file](https://gitlab.ethz.ch/k8s-let/grading-service/otter-grader-service-ansible/-/blob/main/roles/testserver/files/compose/compose.yml?ref_type=heads)
used for integration tests.

**Important**: the integration test works with controlled inputs. The grader instances uses Docker in
Docker and accesses the underlying Docker socket with root permissions. Don't copy this setup to
a production environment! Jupyter notebooks running as root and accessing the host environment
may cause security issues.

**Always run graders from local installations!**

### Kubernetes
*Pusher* and *responder* can be deployed on Kubernetes.

Similar to the issues with Docker grader deployments, running graders in Kubernetes should be used
with caution. Furthermore, Docker in Docker on Kubernetes doesn't use a persistent image cache. This
would lead to long startup times for the usually very large grading images.

## Preparation
### Deployment Type
The variable `DEPLOYMENT_TYPE` is common to all components of the grading service. It is used
as a suffix for some RabbitMQ attributes:

- usernames
- virtual hosts

If usernames and virtual hosts aren't set manually, they use defaults with the *deployment type*
added as suffixes, for example:

- deployment type = `dev`: username *otter-dev*, virtual host *ottergrader-dev*
- deployment type = `test`: username *otter-test*, virtual host *ottergrader-test*
- deployment type = `prod`: username *otter-dev*, virtual host *ottergrader-prod*
- etc.

### RabbitMQ
*RabbitMQ* is configured by configuration files, environment variables are deprecated
now. There are two kinds of configuration files used here:

- *.conf files for program settings, mainly TLS settings
- a JSON file with internals: users and permissions, vhosts, and queues

For a configuration example see the file in `/conf.d` in this repository. Adapt the TLS
certificate paths in `20-local.conf`.

The JSON file must be exported from a running instance. Create a basic RabbitMQ
instance, a Docker based instance is easy to set up. Connect a
terminal session to the Docker container and use `rabbitmqctl` to create

- virtual hosts
- users
- permissions for users on virtual hosts

For example:
- create a virtual host: `rabbitmqctl add_vhost ottergrader-prod`
- create a user: `rabbitmqctl add_user "otter-prod"` and set a password
- assign permissions: `rabbitmqctl set_permissions -p "ottergrader-prod" "otter-prod" ".*" ".*" ".*"`

Access the UI on http://<rabbitmq_hostname>:15671, log in with the default credentials `guest`, `guest`.
On the *Overview* page, scroll down, expand *Export definitions* and download a broker definition file.
This file is the JSON configuration file mentioned above. Passwords are SHA256 hashed and therefore can
be deployed without further encryption.

For further details on the RabbitMQ configuration, visit
[RabbitMQ documentation](https://www.rabbitmq.com/admin-guide.html).

### TLS Certificates
Connections with RabbitMQ are secured by TLS. A server certificate for RabbitMQ and at least one
client certificate is required. Certificates must share the same issuer. For development and tests,
a [self signing CA](https://ypbind.de/maus/ssl_pki_with_openssl.html) is perfectly fine.
