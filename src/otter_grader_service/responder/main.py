import json
import urllib.parse
from importlib.metadata import version

__version__ = version("otter_grader_service")
import sys
import time
import logging
import requests
from otter_grader_service.common.configuration import (
    moodle_host,
    out_token,
    stay_connected,
    drop_undeliverable,
)
from otter_grader_service.common.logging import logger
from otter_grader_service.common.queues import subscribe_results
from otter_grader_service.common.payload import Grading, ResponseMessage

logging.getLogger("pika").setLevel(logging.WARNING)
log = logger("Responder")
log.info(f"Otter grader service v{__version__}")


def main():
    """
    Listen to results queue, process messages and forward to LMS
    :return:
    """

    if out_token is None or len(out_token) == 0:
        log.error("OUT_TOKEN env variable missing")
        exit(1)

    def callback(ch, method, properties, body):
        """
        Process an incoming payload.Grading message
        :param ch: RabbitMQ Channel
        :param method:
        :param properties:
        :param body: Message body (the Grading object)
        :return:
        """

        grading = Grading.model_validate_json(body)
        # trip time measuring
        delta = time.time() - grading.timestamp
        if grading.submission_id == 0:
            log.info(f"Ping received, time elapsed {delta}s")
        else:
            log.info(
                f"Received results for submission {grading.submission_id}, time elapsed {delta}s, result: {grading.rc}"
            )

            # Message object to be sent back to LMS
            msg = ResponseMessage(payload=grading, rc=grading.rc)

            dummy = out_token  # noqa  suppress linting error until actually used
            ver = urllib.parse.quote(__version__)
            ts = int(time.time())
            post_args = json.dumps(
                {
                    "submission_id": msg.payload.submission_id,
                    "rc": msg.rc,
                    "results": msg.payload.results.decode("utf-8"),
                    "version": ver,
                    "ts": ts,
                }
            ).replace(
                " ", ""
            )  # Moodle plugin has issues with whitespace
            post_string = {"input": urllib.parse.quote(post_args)}
            log.info(moodle_host)
            log.debug("Args: " + post_args)
            log.debug("String: " + json.dumps(post_string))
            log.debug("Token: " + out_token[0:3] + "...")
            headers = {
                "access-token": out_token,
                "content-type": "application/x-www-form-urlencoded",
            }
            try:
                res = requests.post(
                    moodle_host, data=post_string, headers=headers, timeout=15
                )
            except requests.exceptions.ReadTimeout:
                # Abort, no ACK
                return
            except requests.exceptions.ConnectionError:
                log.error("Cannot connect to LMS")
                if drop_undeliverable:
                    log.warn(
                        f"Dropping undeliverable task for submission {msg.payload.submission_id}"
                    )
                    ch.basic_ack(delivery_tag=method.delivery_tag)
                else:
                    log.warn(
                        f"Requeing undeliverable task for submission {msg.payload.submission_id}"
                    )
                    ch.basic_reject(delivery_tag=method.delivery_tag)
                return
            log.info(f"LMS response: {res.status_code} {res.text}")

        ch.basic_ack(delivery_tag=method.delivery_tag)

    subscribe_results(callback)
    if stay_connected == "on":
        while True:
            subscribe_results(callback)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        log.info("Interrupted")
        try:
            sys.exit(0)
        except SystemExit:
            exit(0)
