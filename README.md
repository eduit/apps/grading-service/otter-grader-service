# otter-grader-service: Services for Jupyter notebook grading.

## Summary
*otter-grader-service* provides several services for automatic grading of JupyterNotebooks.

The process of autograding uses *otter-grader* to perform the grading. Notebooks have to
be prepared for *otter-grader* and split by `otter generate` into a student version and
a `autograder.zip` containing grading information.

Students get the student version of the notebooks as assignments, and return their solutions
as submission. A submission, together with the `autograder.zip`, will be fed into the grading
process.

The services involved in the grading process are

- a LMS handling assignments, submissions and grading feedback to students and teacher
- *pusher*: an API grading server which receives submissions and the `autograder.zip` and pushes them
  on the grading queue
- a queueing system buffering grading jobs and grading results
- *grader*: one or more grading workers which consume the job messages, grade the submission and enqueue the results
- *responder*: a service which reads the results from the response queue and returns them to the LMS

This repository contains the code for the pusher, the grading worker and the responder.

## Requirements
If using the recommended installation mode for the push and respond services, which is the Docker image,
a host with a Docker runtime is required. Also, Kubernetes works fine. The pusher has no builtin TLS
capabilities, a proxy for TLS termination is recommended.

The grader uses Docker images for grading tasks, hosts running the grader also require a Docker runtime.

Notebook images used for grading must contain all necessary Python libraries used in notebooks to be graded.
They also require the Python package *otter-grader* to be installed.

The images used for grading may get very large, make sure hosts running graders have enough disk space available.

## Hints
### No Inline PDF export
Avoid the otter configuration option to generate an export cell:
```yaml
# ASSIGNMENT CONFIG
export_cell:
  pdf: true
  instructions: ...
```
This option adds a cell which contains a PDF export statement, mainly for self assessment. The automated
grading itself initiates a PDF export, which will eventually collide with the PDF export statement.
