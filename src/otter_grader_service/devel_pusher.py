import otter_grader_service.pusher.main as ogs
import uvicorn

"""
Used for local development.
"""

uvicorn.run(ogs.app, host="localhost", port=8000)
