import base64
import zipfile

import pytest
import os
import shutil
from io import BytesIO
from tempfile import TemporaryDirectory
from otter_grader_service.grading.worker import (
    Job,
    MULTIPLE_NOTEBOOKS,
    NO_NOTEBOOK,
    GRADING_OK,
)
from otter_grader_service.grading.worker_docker import WorkerDocker
from otter_grader_service.common.payload import Submission, Grading

solution_file = open("tests/data/autograder.zip.base64", "rb")
submission_file = open("tests/data/submission.zip.base64", "rb")

submission = Submission(
    submission_id=11111,
    course_id=22222,
    solution=solution_file.read(),
    submission=submission_file.read(),
)

job = Job(submission=submission)


@pytest.fixture
def dir_for_test():
    return TemporaryDirectory()


@pytest.fixture
def worker(dir_for_test):
    return WorkerDocker(working_space_root=dir_for_test.name, current_job=job)


def test_extract_submission(dir_for_test, worker):
    worker.prepare_dir()
    worker.extract_submission()

    assert os.path.isfile(os.path.join(worker.grading_path(), "demo.ipynb"))
    assert os.path.getsize(os.path.join(worker.grading_path(), "demo.ipynb")) > 0


def test_write_grading_info(dir_for_test, worker):
    worker.prepare_dir()
    worker.write_grading_info()

    assert os.path.isfile(os.path.join(worker.grading_path(), "grading.zip"))
    assert os.path.getsize(os.path.join(worker.grading_path(), "grading.zip")) > 0


def test_check_submission(dir_for_test, worker):
    worker.prepare_dir()
    worker.extract_submission()
    worker.check_submission()
    assert job.status == GRADING_OK

    shutil.copy(
        os.path.join(worker.grading_path(), "demo.ipynb"),
        os.path.join(worker.grading_path(), "demo2.ipynb"),
    )
    worker.check_submission()
    assert job.status == MULTIPLE_NOTEBOOKS

    os.remove(os.path.join(worker.grading_path(), "demo.ipynb"))
    os.remove(os.path.join(worker.grading_path(), "demo2.ipynb"))
    worker.check_submission()
    assert job.status == NO_NOTEBOOK


def test_read_grading(dir_for_test, worker):
    worker.current_job = job
    worker.current_job.grading = Grading(submission_id=0, course_id=0, results="")

    worker.prepare_dir()
    shutil.copy("tests/data/demo.pdf", os.path.join(worker.grading_path(), "demo.pdf"))
    shutil.copy(
        "tests/data/results.json", os.path.join(worker.grading_path(), "results.json")
    )
    worker.read_grading()
    assert worker.current_job.grading is not None

    zip_blob = BytesIO(
        initial_bytes=base64.b64decode(worker.current_job.grading.results)
    )
    z = zipfile.ZipFile(zip_blob, "r")
    assert "demo.pdf" in z.namelist() and "results.json" in z.namelist()


# def test_enqueue(dir_for_test, worker):
#     worker.prepare_dir()
#     worker.grade(job)
#     assert job.grading is not None
#
#     zip_blob = BytesIO(base64.b64decode(worker.current_job.grading.results))
#     z = zipfile.ZipFile(zip_blob, "r")
#     assert "demo.pdf" in z.namelist() and "results.json" in z.namelist()
