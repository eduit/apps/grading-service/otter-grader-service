from otter_grader_service.common.logging import logger
import logging
import urllib.parse

logging.getLogger("pika").setLevel(logging.WARNING)
log = logger("Responder")

moodle_url = "abced"
post_args = urllib.parse.urlencode(
    {
        "submission_id": 123456,
        "rc": 0,
        "version": "version X",
        "ts": 1356,
    }
)

log.info(moodle_url + " " + post_args)
