import pathlib
import subprocess  # nosec B404
from otter_grader_service.common.logging import logger
from otter_grader_service.grading.worker import (
    Job,
    Worker,
    GRADING_OK,
    PERMISSION_DENIED,
    OTHER_ERROR,
)
from otter_grader_service.common.payload import Grading
from otter_grader_service.common.configuration import docker_cmd


grading_info_file = "grading.zip"

grade_template = "{docker_cmd} run --rm -ti --mount 'type=bind,src={grade_path},target={grading_home}/{grading_dir}' --name grader-worker-{id} {image} /opt/conda/bin/otter run -a {grading_info} -o {grading_home}/{grading_dir} {notebook}"
export_template = "{docker_cmd} run --rm -ti --mount 'type=bind,src={grade_path},target={grading_home}/{grading_dir}' --name grader-worker-{id} {image} /opt/conda/bin/otter export {notebook}"

log = logger(__name__)


class WorkerDocker(Worker):
    """
    Otter autograder running in a Docker environment.

    Run on a server able to launch Docker containers. Bind mounts are used, make sure the
    process has write permissions in configuration.grading_home (also SElinux!)
    """

    working_space_root: str = ""
    current_job: Job = None

    def grade(self, job: Job):
        """
        Perform grading. Writes files submitted, runs grader, reads grading result and
         reports back in queue, and cleans up the grading directory.
        :param job: Job object with submission and solution information
        :return: None
        """
        self.current_job = job
        self.current_job.grading = Grading(submission_id=0, course_id=0, results="")
        self.prepare_dir()  # create directories
        self.write_grading_info()  # dump ZIP with test and solutions
        self.extract_submission()  # extract ZIP with notebook (and data)
        self.check_submission()  # input files check

        # Now do the grading
        if self.current_job.status == GRADING_OK:
            # Grading job
            # Don't use pathlib, os.path.join etc.! Paths inside container are always Linux,
            # we don't care what the host system is like.
            grade_cmd = [
                docker_cmd,
                "run",
                "--rm",
                "-i",
                "--mount",
                "type=bind,src={grade_path},target=/home/jovyan/{grading_dir}".format(
                    grade_path=pathlib.Path(self.grading_path()),
                    grading_dir=self.grading_dir,
                ),
                "--name",
                "grader-worker-{0}".format(self.current_job.id),
                self.current_job.image,
                "/opt/conda/bin/otter",
                "run",
                "-a",
                self.grading_dir + "/" + grading_info_file,
                "-o",
                "/home/jovyan/" + self.grading_dir,
                self.grading_dir + "/" + self.current_job.notebook_name + ".ipynb",
            ]
            log.info(
                f"Starting grading for submission {self.current_job.submission.submission_id}"
            )
            # Launch generation of results.json
            cp = None
            try:
                cp = subprocess.run(
                    grade_cmd, capture_output=True, encoding="utf-8"
                )  # nosec B603
            except FileNotFoundError as e:
                # Docker cmd not found
                log.fatal("Grade run failed (docker cmd not found?): {0}".format(e))
                exit(1)

            if cp.returncode != 0:
                if "PermissionError" in cp.stderr:
                    log.debug("'PermissionError' found in job output")
                    self.current_job.status = PERMISSION_DENIED
                else:
                    self.current_job.status = OTHER_ERROR
                self.current_job.log = cp.stderr
                log.error(
                    "Grading for submission {0} failed.".format(
                        self.current_job.submission.submission_id
                    )
                )
                log.error(cp.stderr)
                self.finalize()
                return

            # Command for PDF generation
            grade_cmd = [
                docker_cmd,
                "run",
                "--rm",
                "-i",
                "--mount",
                "type=bind,src={grade_path},target=/home/jovyan/{grading_dir}".format(
                    grade_path=pathlib.Path(self.grading_path()),
                    grading_dir=self.grading_dir,
                ),
                "--name",
                "grader-worker-{0}".format(self.current_job.id),
                self.current_job.image,
                "/opt/conda/bin/otter",
                "export",
                "/home/jovyan/"
                + self.grading_dir  # noqa W503
                + "/"  # noqa W503
                + self.current_job.notebook_name  # noqa W503
                + ".ipynb",  # noqa W503
            ]
            log.info(
                f"Starting PDF export for submission {self.current_job.submission.submission_id}"
            )
            # Launch generation of PDF
            cp = subprocess.run(
                grade_cmd, capture_output=True, encoding="utf-8"
            )  # nosec B603
            if cp.returncode != 0:
                if "PermissionError" in cp.stderr:
                    self.current_job.status = PERMISSION_DENIED
                else:
                    self.current_job.status = OTHER_ERROR
                self.current_job.log = cp.stderr
                log.error(
                    "PDF export for submission {0} failed.".format(
                        self.current_job.submission.submission_id
                    )
                )
                self.finalize()

            log.info(
                "Grading for submission {0} done.".format(
                    self.current_job.submission.submission_id
                )
            )

            self.read_grading()  # read results files

        self.finalize()
