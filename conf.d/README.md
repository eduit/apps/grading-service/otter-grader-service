# Configuration for RabbitMQ

Use these files as a starting point for the RabbitMQ configuration. Using Docker,
the directory can be mounted over `/etc/rabbitmq/conf.d`. This will shadow the standard
configuration file `10-default.conf`, because of that an original file is present
here. Adapt the file `20-local.conf` to your environment, mainly the TLS file paths.

It also forces RabbitMQ to read the schema from `config.json`, which can be used to configure
vhosts, queues and users as a starting point:

- Create an instance of RabbitMQ in a Docker environment. It comes up with a default
  user and password, *guest/guest*.
- Use the container console to [create a user](https://www.rabbitmq.com/access-control.html)
  with a secure password.
- [Export the configuration](https://www.rabbitmq.com/definitions.html), i.e. on the status
  overview page of the management UI.
- Replace the hashed password in the original configuration.
