# Configuration
All services are configured by environment variables.

For the configuration of *RabbitMQ*, take a look at `20-local.conf`. The only options
to be adapted are the TLS certificate paths.

## Common Variables
### `DEPLOYMENT_TYPE`
Default: `dev`

The intention of this variable is explained in the [installation documentation](install.md).

### `LOGLEVEL`
Default: `INFO`

Standard log level settings: `DEBUG`, `WARN`

### `CERT_DIRECTORY`
Default: `/etc/rabbitmq/certs`

Base directory where certificates are stored.

### `RABBITMQ_HOST`
Default: `localhost`

### `RABBITMQ_CA_FILE`
Default: not set

File containing the certification authority certificate chain (one or more certificates).

### `RABBITMQ_CERT_FILE`
Default: not set

TLS certificate.

### `RABBITMQ_KEY_FILE`
Default: not set

Key for the TLS certificate; no password.

### `RABBITMQ_VIRTUALHOST`
Default: ottergrader-${DEPLOYMENT_TYPE}

Used by *RabbitMQ* to separate users and queues, similar to namespaces.

### `RABBITMQ_USER`
Default: otter-${DEPLOYMENT_TYPE}

Used in combination with the password below, grants access to the virtual host defined above.

### `RABBITMQ_PASSWORD`
Default: not set

### `STAY_CONNECTED`
Default: 'on'

Retry if connection to *RabbitMQ* is lost. Set to 'off' if a connection loss should cause the service to exit
with an error message.

## Pusher Specific
### `IN_TOKEN`
Default: not set

Access token for the transfer from Moodle to the grading service.

## Grader Specific
### `DOCKER_CMD`
Default: `docker`

Command to launch docker.

### `GRADING_HOME`
Default: `/home/jovyan`

During grading, the notebook with optional files and the grading archive are extracted
into a temporary directory below `GRADING_HOME`. The Docker container used for grading
mounts this directory and writes back the grading result and PDF files. Finally, the
grading process collects these files.

The directory must be writeable for user ID 1000, the ID used in Jupyter images.

### `GRADING_IMAGE`
Default: registry.ethz.ch/k8s-let/notebooks/jh-notebook-universal:3.0.0-36

Image used to grade notebooks. Must contain the Python libraries used in the notebooks
and an installation of *otter*.

## Responder specific
### `MOODLE_HOST`
Default: http://localhost:8001

URL used to feed results back into Moodle.

### `OUT_TOKEN`
Default: not set

Access token for the transfer from the grading service to Moodle.
