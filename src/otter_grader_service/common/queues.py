import pika
import ssl
import time
import pathlib
from pika.exceptions import (
    AMQPConnectionError,
    AuthenticationError,
    StreamLostError,
    ProbableAccessDeniedError,
    ConnectionClosedByBroker,
)
from starlette.exceptions import HTTPException as StarletteHTTPException

from .logging import logger
from .configuration import (
    rabbitmq_host,
    rabbitmq_virtualhost,
    rabbitmq_user,
    rabbitmq_password,
    stay_connected,
    cert_directory,
    rabbitmq_cert_file,
    rabbitmq_key_file,
    rabbitmq_ca_file,
)

log = logger(__name__)

grading_queue_name = "grading"
results_queue_name = "results"

connection: pika.BlockingConnection = None
channel: pika.adapters.blocking_connection.BlockingChannel = None


class Certificates:
    """
    Test certificate availability
    """

    key: pathlib.Path
    cert: pathlib.Path
    ca = pathlib.Path

    def __init__(self):
        c = pathlib.Path(cert_directory)
        self.key = c / rabbitmq_key_file
        self.cert = c / rabbitmq_cert_file
        self.ca = c / rabbitmq_ca_file

        # Check if files are present and can be read
        for path in [self.key, self.cert, self.ca]:
            log.debug(f"Certificate configured: {path}")
            try:
                f = path.open()
                f.readline()
                f.close()
            except FileNotFoundError:
                log.fatal(f"Could not open {path}")
                exit(1)


certs = Certificates()


def connect(force_reconnect=False):
    """
    Connect to RabbitMQ service.

    If stay_connected is set via configuration, disconnects are caught and connections retried
    after 10 seconds, looping forever or until the process gets killed.
    :param force_reconnect: force channel recreation
    :return: connected channel (a server still may vanish before the next code line)
    """
    global connection
    global channel

    context = ssl.create_default_context(
        purpose=ssl.Purpose.SERVER_AUTH, cafile=certs.ca
    )
    context.verify_mode = ssl.CERT_REQUIRED
    context.check_hostname = False

    context.load_cert_chain(certs.cert, certs.key)
    ssl_options = pika.SSLOptions(context, rabbitmq_host)
    credentials = pika.PlainCredentials(rabbitmq_user, rabbitmq_password)

    # Open connection if not yet done
    if connection is None or connection.is_closed:
        while True:
            try:
                connection = pika.BlockingConnection(
                    pika.ConnectionParameters(
                        host=rabbitmq_host,
                        virtual_host=rabbitmq_virtualhost,
                        credentials=credentials,
                        port=5671,
                        heartbeat=600,
                        ssl_options=ssl_options,
                    )
                )
            except AuthenticationError:
                log.fatal("Queue authentication failed")
                raise StarletteHTTPException(500)  # go and shutdown async fastapi
            except ProbableAccessDeniedError:
                log.fatal("Queue access denied (check vhost)")
                raise StarletteHTTPException(500)
            except (
                AMQPConnectionError,
                ConnectionClosedByBroker,
                ssl.SSLEOFError,
                StreamLostError,
            ) as e:
                if stay_connected != "on":
                    log.error("Connection to queue failed.")
                    raise e
                else:
                    log.error("Connection to queue failed, pausing concection attempt.")
                    connection = None
                    time.sleep(10)
            if connection is not None:
                break

    # Open channel and create queues
    if channel is None or not channel.connection.is_open or force_reconnect:
        channel = connection.channel()
        channel.queue_declare(queue=grading_queue_name)
        channel.queue_declare(queue=results_queue_name)

    log.info("Connected to queue.")
    return channel


def push_object(obj, queue_name):
    """
    Push an object (pydantic) to the queue.
    :param obj: Object to be pushed to the queue.
    :param queue_name: For gradings or results
    :return:
    """
    log.debug("push_object: trying to connect")
    ch = connect()
    log.debug("push_object: connected; trying to publish")
    try:
        ch.basic_publish(
            exchange="", routing_key=queue_name, body=obj.model_dump_json()
        )
        log.debug("push_object: pushed, ok")
    except (
        AMQPConnectionError,
        ConnectionClosedByBroker,
        ssl.SSLEOFError,
        StreamLostError,
    ) as e:
        log.debug("push_object: push failed")
        log.error(e)
        # Retry connection if configured to do so
        if stay_connected != "on":
            log.error("Connection to queue failed.")
            raise StarletteHTTPException(500)


def push_job(obj):
    """
    Push a submission for grading to the grading queue.
    :param obj: a payload.Submission object
    :return:
    """
    push_object(obj, grading_queue_name)


def push_result(obj):
    """
    Push a grading result to the results queue.
    :param obj: a payload.Grading object
    :return:
    """
    push_object(obj, results_queue_name)


def subscribe_object(callback_fct, queue_name):
    """
    Connect to the queue and wait for messages. Catches disconnects if configuration.stay_connected is set.
    :param callback_fct: callback function which processes the message
    :param queue_name: connect to this queue
    :return: RabbitMQ channel
    """
    ch = connect()
    try:
        ch.basic_consume(
            queue=queue_name, on_message_callback=callback_fct, auto_ack=False
        )
        log.info("Waiting for messages.")
        ch.start_consuming()
    except (
        AMQPConnectionError,
        ConnectionClosedByBroker,
        ssl.SSLEOFError,
        StreamLostError,
    ):
        log.error("Connection to queue failed.")
        # Retry connection if configured to do so
        if stay_connected != "on":
            raise StarletteHTTPException(500)

    return ch


def unsubscribe():
    """
    Stop listening for new messages
    :return:
    """
    channel.stop_consuming()


def subscribe_results(callback_fct):
    """
    Start listening to the results queue.
    :param callback_fct: callback function which processes the message
    :return: Channel
    """
    return subscribe_object(callback_fct, results_queue_name)


def subscribe_jobs(callback_fct):
    """
    Start listening to the grading queue.
    :param callback_fct: callback function which processes the message
    :return: Channel
    """
    return subscribe_object(callback_fct, grading_queue_name)
