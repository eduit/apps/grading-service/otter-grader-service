from pydantic import BaseModel
from fastapi import HTTPException

from .logging import logger
from .configuration import in_token

log = logger(__name__)


class Payload(BaseModel):
    """
    Model of the base payload part of the HTTP request
    """

    rc: int = 0
    submission_id: int
    course_id: int

    timestamp: float = 0.0  # internally added, to monitor roundtrip time


class Submission(Payload):
    """
    Payload for submissions (incoming stuff)
    """

    solution: str
    submission: str


class Grading(Payload):
    """
    Payload for grading results (outgoing stuff)
    """

    results: bytes


class ResponseMessage(BaseModel):
    payload: Grading
    rc: int = 0
    msg: str = "OK"


# SecureMessage removed after 0.0.33


class TokenMessage(BaseModel):
    """
    Message for simple token based authentication.
    """

    payload: Submission
    token: str
    rc: int = 0
    msg: str = "OK"

    def validate_all(self):
        if self.token != in_token:
            raise HTTPException(
                status_code=403,
                detail={"return": {"code": 403, "status": "Authentication error"}},
            )
